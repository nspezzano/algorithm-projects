nterms = int(input("How many terms? "))
n1 = 0
n2 = 1
count = 2

if nterms <= 0:
   print("Plese enter a positive integer")
elif nterms == 1:
   print("Fibonacci sequence(iteration):")
   print(n1)
else:
   print("Fibonacci sequence(iteration):")
   print(n1,",",n2,end=', ')
   while count < nterms:
       nth = n1 + n2
       print(nth,end=' , ')
       # update values
       n1 = n2
       n2 = nth
       count += 1
       
def recur_fibo(n):
   if n <= 1:
       return n
   else:
       return(recur_fibo(n-1) + recur_fibo(n-2))

if nterms <= 0:
   print("Plese enter a positive integer")
else:
   print("\n Fibonacci sequence(recursive):")
   for i in range(nterms):
       print(recur_fibo(i))
"""
I notice that the fib function using iterations is faster and more efficient than the
recursive function.Typing in a fewer amount of terms I saw that the speed is similar.
When I typed in a big number of terms (for example I did 90) I noticed that the iteration
function output the numbers very fast. When it was time for the recursive function
to output the numbers, it output the first 30 or so pretty fast. After 30, it started totake a long time for it to spit the numbers out. I can
prove this is true by running my program.
"""
